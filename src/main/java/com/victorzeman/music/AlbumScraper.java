package com.victorzeman.music;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by victo on 4/16/2017.
 */
public class AlbumScraper {

    String PHANTOM_JS_BINARY = "C:\\Program Files\\PhantomJS\\phantomjs.exe";
    String CHROME_BINARY = "C:\\Program Files (x86)\\Chromedriver\\chromedriver.exe";

    private String url;
    private static WebDriver driver;

    public AlbumScraper(String url){
        if (driver == null) {
            DesiredCapabilities caps = new DesiredCapabilities();
            String[] phantomArgs = new String[]{
                    "--webdriver-loglevel=NONE"
            };
            Logger.getLogger(PhantomJSDriverService.class.getName()).setLevel(Level.OFF);
            Logger.getLogger(WebDriver.class.getName()).setLevel(Level.OFF);

            caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, phantomArgs);
            caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, PHANTOM_JS_BINARY);
            driver = new PhantomJSDriver(caps);
            //System.setProperty("webdriver.chrome.driver", CHROME_BINARY);
            //this.driver = new ChromeDriver();

        }
        this.url = url;
    }

    public String getUrl(){
        return this.url;
    }
    public void setUrl(String url){
        this.url = url;
    }

    public String findAlbumArtImageUrl(String url){
        System.out.println("Downloading from: " + url);
        driver.get(this.url);
        System.out.println("On URL: " + driver.getCurrentUrl());


        try {
            String resultCss = "#mantle_skin > div.container.page-content > div > div.col-main > section:nth-child(4) > ol > li:nth-child(1) > div.grid-items-cover-image.js-link-block.link-block > div.grid-items-item-details > p.grid-items-item-main-text > a";
            WebElement firstResultFromSearch = (new WebDriverWait(driver, 3).until(ExpectedConditions.elementToBeClickable(By.cssSelector(resultCss))));
            firstResultFromSearch.click();
            System.out.println("ON URL: " + driver.getCurrentUrl());
            Thread.sleep(50);
            try {
                //#content > div:nth-child(2) > header > div.container > div > div.header-avatar > div > div > a > img
                String profileImageCss = "#content > div:nth-child(4) > header > div.container > div > div.header-avatar > div > div > a";
                WebElement profileImage = (new WebDriverWait(driver, 3).until(ExpectedConditions.elementToBeClickable((By.cssSelector(profileImageCss)))));
                profileImage.click();
                System.out.println("ON URL: " + driver.getCurrentUrl());
                Thread.sleep(50);

                WebElement image = (new WebDriverWait(driver, 3).until(ExpectedConditions.elementToBeClickable(By.cssSelector("#mantle_skin > div.content-top > div > div.container.content-top-lower > section > div > div.col-main > section > div.gallery-image-container.embed-responsive.embed-responsive-4by3 > div > a:nth-child(2) > img"))));
                String imageUrl = image.getAttribute("src");
                System.out.println("Found image url! : " + imageUrl);

                return imageUrl;
            } catch( Exception e){
                String profileImageCss = "#content > div:nth-child(2) > header > div.container > div > div.header-avatar > div > div > a > img";
                WebElement profileImage = (new WebDriverWait(driver, 3).until(ExpectedConditions.elementToBeClickable((By.cssSelector(profileImageCss)))));
                profileImage.click();
                System.out.println("ON URL: " + driver.getCurrentUrl());
                Thread.sleep(50);
                System.out.println(e);

                WebElement image = (new WebDriverWait(driver, 3).until(ExpectedConditions.elementToBeClickable(By.cssSelector("#mantle_skin > div.content-top > div > div.container.content-top-lower > section > div > div.col-main > section > div.gallery-image-container.embed-responsive.embed-responsive-4by3 > div > a:nth-child(2) > img"))));
                String imageUrl = image.getAttribute("src");
                System.out.println("Found image url! : " + imageUrl);

                return imageUrl;
            }
        }catch(Exception nse) {
            //System.out.println(nse);
            System.out.println("Could not retrieve album art (404)");
            return "";
        }
    }

    public void downloadArtistImage(String folderDownloadPath) throws IOException{
        String url = findArtistImageUrl(this.url);

        if (StringUtils.isNotBlank(url)){
            Image image = null;
            URL urlobj = new URL(url);
            image = ImageIO.read(urlobj);
            InputStream in = new BufferedInputStream(urlobj.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n;
            while(-1!=(n=in.read(buf))){
                out.write(buf,0,n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();
            File file = new File(folderDownloadPath + "/folder.jpg");
            if (file.exists()){
                file.delete();
            }
            FileOutputStream fos = new FileOutputStream(folderDownloadPath + "/folder.jpg", false);
            fos.write(response);
            fos.close();
        }else {
            System.out.println("Could not find image for " + folderDownloadPath);
        }
    }

    private String findArtistImageUrl(String url) throws IOException{
        System.out.println("Downloading from: " + url);
        driver.get(this.url);
        System.out.println("On URL: " + driver.getCurrentUrl());

        try {
            String cssProfileSelector = "#content > div:nth-child(4) > header > div.container > div > div.header-avatar > div > a";
            WebElement profileImage = (new WebDriverWait(driver, 6).until(ExpectedConditions.elementToBeClickable((By.cssSelector(cssProfileSelector)))));
            profileImage.click();
            System.out.println("ON URL: " + driver.getCurrentUrl());

        }catch(Exception e){
            String cssProfileSelector = "#content > div:nth-child(2) > header > div.container > div > div.header-avatar > div > a > img";
            WebElement profileImage = (new WebDriverWait(driver, 6).until(ExpectedConditions.elementToBeClickable((By.cssSelector(cssProfileSelector)))));
            profileImage.click();
            System.out.println("ON URL: " + driver.getCurrentUrl());

        }
        try{
            String artistImageSelector = "#mantle_skin > div.content-top > div > div.container.content-top-lower > section > div > div.col-main > section > div.gallery-image-container.embed-responsive.embed-responsive-4by3 > div > a:nth-child(2) > img";
            WebElement artistImage = (new WebDriverWait(driver, 6).until(ExpectedConditions.elementToBeClickable((By.cssSelector(artistImageSelector)))));
            String imageUrl = artistImage.getAttribute("src");
            System.out.println("Artist image downloaded!");

            return imageUrl;
        }catch(Exception e){
            System.out.println("ON URL: " + driver.getCurrentUrl());

            return "";
        }
    }

    public void downloadAlbumArtImage(String folderDownloadPath)  {
        String url = findAlbumArtImageUrl(this.url);
        try {
            if (StringUtils.isNotBlank(url)) {
                Image image = null;
                URL urlobj = new URL(url);
                image = ImageIO.read(urlobj);
                InputStream in = new BufferedInputStream(urlobj.openStream());
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];
                int n;
                while (-1 != (n = in.read(buf))) {
                    out.write(buf, 0, n);
                }
                out.close();
                in.close();
                byte[] response = out.toByteArray();
                File file = new File(folderDownloadPath + "/folder.jpg");
                if (file.exists()) {
                    file.delete();
                }
                FileOutputStream fos = new FileOutputStream(folderDownloadPath + "/folder.jpg", false);
                fos.write(response);
                fos.close();
            } else {
                System.out.println("Could not find image for " + folderDownloadPath);
            }
        }catch(IOException e){
            System.out.println("Failed to download album art: " + e);
        }
    }
}
