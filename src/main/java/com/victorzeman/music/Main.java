package com.victorzeman.music;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by victo on 4/16/2017.
 */
public class Main {

    //private static String EXECUTION_PATH = "\\\\Steven-Pc\\Music";

    private static String EXECUTION_PATH = "D:\\Music";

    public static void main(String args[]){

        long i = 0;

        System.out.println("Current directory: " + System.getProperty("user.dir"));
        //String currentDir = System.getProperty("user.dir");
        File file = new File(EXECUTION_PATH);
        for(String artistName: file.list()){
            File artistFolder = new File(EXECUTION_PATH + "\\" + artistName);
            //System.out.println("Dir? " + artistFolder.isDirectory() + ":" + artistFolder);
            if (artistFolder.isDirectory()) {
                for (String albumName : artistFolder.list()) {
                    File albumFolder = new File(artistFolder.getAbsoluteFile() + "\\" + albumName);
                    //System.out.println("__Dir? " + albumFolder.isDirectory() + ":" + albumFolder);
                    if (albumFolder.isDirectory()) {
                        String formattedAlbumName = null;
                        try {
                            formattedAlbumName = albumName.split("\\d\\d. ")[1].split("[\\[]")[0];
                            List<String> albumFiles = Arrays.asList(albumFolder.list());
                            if (albumFiles.contains(formattedAlbumName + ".jpg") || getMatchingStrings(albumFiles, "[Ff]older.png").size() > 0 || getMatchingStrings(albumFiles,"[f]older.jpg").size() > 0 || getMatchingStrings(albumFiles,"[Cc]over.*").size() > 0 || getMatchingStrings(albumFiles,"[Ff]ront.*").size() > 0){
                                //System.out.println("____Artwork found!! for " + formattedAlbumName);
                            }else{
                                //System.out.println("____No Artwork :'( for " + formattedAlbumName);
                                //Retrieve artwork!
                                //System.out.println("Retrieving artwork for query=> " + artistName + ":" + formattedAlbumName);
                                String url = "https://www.last.fm/search?q=" + URLEncoder.encode(formattedAlbumName, "UTF-8") + "+" + URLEncoder.encode(artistName, "UTF-8");
                                //String url = "https://www.last.fm/music/" + URLEncoder.encode(artistName,  "UTF-8") + "/" + URLEncoder.encode(formattedAlbumName, "UTF-8") + "/+images/";
                                AlbumScraper scraper = new AlbumScraper(url);
                                scraper.downloadAlbumArtImage(albumFolder.getAbsolutePath());
                                i++;
                            }
                        }catch(IndexOutOfBoundsException|IOException e) {
                            System.out.println("Album Folder named weirdly." + albumName + " :::" + e);
                        }
                    }
                }
            }
        }
        for(int loop = 1; loop <= 2; loop++){
            if (file.list().length > 0)
            for(String artistName: file.list()) {
                File artistFolder = new File(EXECUTION_PATH + "\\" + artistName);
                if (artistFolder.isDirectory()) {
                    List<String> artistFiles = Arrays.asList(artistFolder.list());
                    String artistFile = "";
                    boolean artistImageFound = false;
                        if (artistFiles.contains("artist.jpg")){
                            artistImageFound = true;
                            artistFile = "artist.jpg";
                        }
                        if (!artistImageFound)
                            if (getMatchingStrings(artistFiles, "[Ff]older.png").size() > 0){
                                artistFile = getMatchingStrings(artistFiles, "[Ff]older.png").get(0);
                                artistImageFound = true;
                            }
                        if (!artistImageFound)
                            if (getMatchingStrings(artistFiles, "folder.jpg").size() > 0){
                                artistFile = getMatchingStrings(artistFiles, "folder.jpg").get(0);
                                artistImageFound = true;
                            }
                    File artistImage = new File(artistFolder + "\\" + artistFile);
                    switch(loop){
                        case 1:
                            if (!artistImageFound){
                                System.out.println("Artist image not found! :( :" + artistName);
                                try{
                                    String url = "https://www.last.fm/music/" + URLEncoder.encode(artistName, "UTF-8");
                                    AlbumScraper scraper = new AlbumScraper(url);
                                    scraper.downloadArtistImage(artistFolder.getAbsolutePath());
                                    i++;

                                }catch(IOException e){
                                    System.out.println(e);
                                }
                            }
                            break;
                        case 2:
                            if (artistImageFound){
                                //System.out.println("Beginning file size check..." + artistName);
                                long artistImageSize = artistImage.length();
                                for(String albumName : artistFolder.list()){

                                    File albumFolder = new File(artistFolder.getAbsoluteFile() + "\\" + albumName);
                                    boolean albumImageFound = false;
                                    String albumFile = "";
                                    if (albumFolder.isDirectory()){

                                        albumImageFound = false;
                                        List<String> albumFiles = Arrays.asList(albumFolder.list());

                                        if (getMatchingStrings(albumFiles, "[Ff]ront.*").size() > 0){
                                            albumFile = getMatchingStrings(albumFiles, "[Ff]ront.*").get(0);
                                            albumImageFound = true;
                                        }
                                        if (!albumImageFound)
                                            if (getMatchingStrings(albumFiles, "[Ff]older.png").size() > 0){
                                                albumFile = getMatchingStrings(albumFiles, "[Ff]older.png").get(0);
                                                if (albumFiles.contains("folder.jpg")){
                                                    new File(albumFolder + "\\folder.jpg").delete();
                                                }
                                                if (albumFiles.contains("cover.jpg")){
                                                    new File(albumFolder + "\\cover.jpg").delete();
                                                }
                                                albumImageFound = true;
                                            }
                                        if (!albumImageFound)
                                            if (getMatchingStrings(albumFiles, "[Cc]over.*").size() > 0){
                                                albumFile = getMatchingStrings(albumFiles, "[Cc]over.*").get(0);
                                                albumImageFound = true;
                                            }
                                        if (!albumImageFound)
                                            if (albumFiles.contains("folder.jpg")){
                                                albumFile = "folder.jpg";
                                                albumImageFound = true;
                                            }
                                        if (albumImageFound){
                                            File albumArtFile = new File(albumFolder + "\\" + albumFile);
                                            long albumArtSize = albumArtFile.length();
                                            if (albumArtSize < artistImageSize){
                                                System.out.println("converting album art to png: " + albumArtFile);
                                                //convert albumArtSize to png.
                                                try {
                                                    BufferedImage bufferedImage = ImageIO.read(albumArtFile);
                                                    ImageIO.write(bufferedImage, "png", new File(albumFolder + "\\folder.png"));

                                                    // this writes the bufferedImage into a byte array called resultingBytes
                                                    ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
                                                    ImageIO.write(bufferedImage, "png", byteArrayOut);

                                                    File newPng = new File(albumFolder + "\\folder.png");
                                                    i++;
                                                    if (artistImageSize < newPng.length()){
                                                        System.out.println("Done");
                                                    } else{
                                                        System.out.println("Converted to PNG but png still too small: " + newPng.length() + " vs " + artistImageSize);
                                                    }

                                                    if (albumArtFile.getName().contains(".jpg"))
                                                        albumArtFile.delete();


                                                }catch(IOException e){
                                                    System.out.println("Failed");
                                                }

                                            }else {
                                                System.out.println("Don't need to convert " + albumArtFile + " to png.");
                                            }
                                        } else{
                                            //System.out.println(">>Artist image not found! " + albumFolder + "::" + albumFiles);
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }
        System.out.println("Completed with " + i + " downloads.");
    }

    /**
     * Finds the index of all entries in the list that matches the regex
     * @param list The list of strings to check
     * @param regex The regular expression to use
     * @return list containing the indexes of all matching entries
     */
    private static List<String> getMatchingStrings(List<String> list, String regex) {

        ArrayList<String> matches = new ArrayList<String>();

        Pattern p = Pattern.compile(regex);

        for (String s:list) {
            if (p.matcher(s).matches()) {
                matches.add(s);
            }
        }
        return matches;
    }
}
